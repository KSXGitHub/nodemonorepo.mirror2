export = Object.freeze([
  '.js',
  '.js.map',
  '.jsx',
  '.jsx.map',
  '.d.ts',
  '.d.ts.map',
  '.d.tsx',
  '.d.tsx.map'
])

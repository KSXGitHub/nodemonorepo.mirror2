# available-npm-name

Check if any package name is taken

## Usage

```sh
available-npm-name package-name-1 package-name-2 ...
```

## License

[MIT](https://git.io/vhaEz) © [Hoàng Văn Khải](https://github.com/KSXGitHub)
